import { RENDER_CONTROLLER } from "./render_controller.js";

export let addEventListener_CONTROLLER = {
  try_virtualGlasses: (array) => {
    array.map((item, index) => {
      document
        .querySelectorAll(".glasses")
        [index].addEventListener("click", () => {
          RENDER_CONTROLLER.render_virtual_glasses(item.virtualImg);
          RENDER_CONTROLLER.render_glasses_info(
            item.name,
            item.brand,
            item.color,
            item.price,
            item.description
          );
        });
    });
  },
};
