export let RENDER_CONTROLLER = {
  render_product: (array) => {
    let contentHTML = "";
    array.map((item) => {
      let contentRendered = ` <div class="glasses">
        <img src="${item.src}" alt="" />
      </div>`;
      contentHTML += contentRendered;
    });
    return contentHTML;
  },
  render_virtual_glasses: (vGlasses) => {
    document.getElementById("avatar").innerHTML = `<div id="glasses">
      <img src="${vGlasses}" alt="" />
      </div>`;
    let glasses = document.getElementById("glasses");
    glasses.style.opacity = 0;
    setTimeout(() => {
      glasses.style.opacity = 1;
    }, 1);
  },
  render_glasses_info: (name, brand, color, price, description) => {
    let glassesInfo = document.getElementById("glassesInfo");
    glassesInfo.style.display = "none";
    glassesInfo.innerHTML = `  <div>
    <h3>${name} - ${brand} (${color})</h3>
   <div class="py-2">
   <button class="btn btn-danger">$${price}</button>
   <span class="text-success">Stocking</span>
   </div>
    <p>
      ${description}
    </p>
  </div>`;
    setTimeout(() => {
      glassesInfo.style.removeProperty("right");
      glassesInfo.style.removeProperty("display");
    }, 1);
    setTimeout(() => {
      glassesInfo.style.right = 0;
    }, 100);
  },
};
