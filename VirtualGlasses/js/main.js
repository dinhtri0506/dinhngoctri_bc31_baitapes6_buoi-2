import { addEventListener_CONTROLLER } from "./controller/addEventListener_controller.js";
import { RENDER_CONTROLLER } from "./controller/render_controller.js";
import { dataGlasses } from "./models/dataGlasses.js";

let productRender = RENDER_CONTROLLER.render_product(dataGlasses);
document.getElementById("vglassesList").innerHTML = productRender;

let try_virtualGlasses =
  addEventListener_CONTROLLER.try_virtualGlasses(dataGlasses);

let removeGlasses = (condition) => {
  let glasses = document.getElementById("glasses");
  if (condition) {
    glasses.style.opacity = 1;
  } else {
    glasses.style.opacity = 0;
  }
};
window.removeGlasses = removeGlasses;
